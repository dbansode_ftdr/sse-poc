import React, {useEffect, useState} from 'react';
import './App.css';

const url = 'http://localhost:8080/events/'
function App() {
  const [message, setMessage] = useState<any[]>([])
  useEffect(()=> {
    const sse = new EventSource(`${url}`);

    sse.onmessage = (msg) => {
      console.log("message:", msg)
      setMessage((prev) => [...prev, msg])
    };
    sse.onerror = function() {
      sse.close();
    };
  },[])
  return (
    <div className="App">
      {message.map(message => {
        return <div>{message.data}</div>
      })}
    </div>
  );
}

export default App;
