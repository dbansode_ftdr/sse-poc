package handlers

import (
	"encoding/json"
	"net/http"
)

func Notification() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		res := "Hello Notifications"
		WriteResponse(w, http.StatusOK, res)
	}
}


func WriteResponse(w http.ResponseWriter, status int, res interface{}) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	json.NewEncoder(w).Encode(res)
}