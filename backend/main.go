package main

import (
	"fmt"
	"golang.frontdoorhome.com/software/poc/sse-poc/backend/handlers"
	"log"
	"net/http"
	"time"

	muxHandler "github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

func main() {
	// Make a new Broker instance
	b := handlers.NewBroker()

	// Start processing events
	b.Start()

	// Generate a constant stream of events that get pushed
	// into the Broker's messages channel and are then broadcast
	// out to any clients that are attached.
	go func() {
		for i := 0; ; i++ {

			// Create a little message to send to clients,
			// including the current time.
			message := fmt.Sprintf("%d - the time is %v", i, time.Now())
			b.PushNotification(message)
			// Print a nice log message and sleep for 5s.
			log.Printf("Sent message %d ", i)
			time.Sleep(5e9)

		}
	}()

	r := mux.NewRouter()
	r.Use(mux.CORSMethodMiddleware(r))

	r.HandleFunc("/notifications/", handlers.Notification()).Methods(http.MethodGet)
	r.Handle("/events/", b).Methods(http.MethodGet)

	log.Fatal(http.ListenAndServe(":8080", muxHandler.CORS(muxHandler.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Authorization"}), muxHandler.AllowedMethods([]string{"GET", "POST", "PUT", "HEAD", "OPTIONS"}), muxHandler.AllowedOrigins([]string{"*"}))(r)))
}
